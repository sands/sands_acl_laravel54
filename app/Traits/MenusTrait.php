<?php

namespace App\Traits;

use App\Http\Library\Serializer;
use App\Models\Menu;
use App\Models\Permission;
use App\Status;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

trait MenusTrait
{
    public function menuInit()
    {
        $user = Auth::user();
        $menus = Menu::all();
        $permissions = Permission::where('name','like','%show%')->orderBy('id','asc')->get();

        $permissions = $permissions->filter(function($value,$key) use ($user){
            return $user->can($value->name);
        })->values();

        //get id permission
        $permissions_id = [];
        foreach($permissions as $permission){
            $permissions_id[] = $permission->id;
        }

        $menus_parent = $menus->filter(function($v, $k) use ($permissions_id){
            $check_have_child = Menu::where('parent_id', $v->id)->first();
            $v->have_child = !empty($check_have_child) ?  true : false;
            $in_array = in_array($v->permission_id, $permissions_id);
            if($in_array && $v->parent_id == 0) return $v;
        });

        $menus_child = $menus->filter(function($v, $k) use ($permissions_id){
            $in_array = in_array($v->permission_id, $permissions_id);
            if($in_array && $v->parent_id != 0) return $v;
        });

        $data = [
            'menus_parent' => $menus_parent,
            'menus_child' => $menus_child,
        ];

        return $data;
    }
}