<?php

namespace App\Models;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    public function module(){
        return $this->belongsTo('App\Models\Module','module_id');
    }
}
