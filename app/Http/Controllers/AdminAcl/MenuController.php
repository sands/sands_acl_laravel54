<?php

namespace App\Http\Controllers\AdminAcl;

use App\Models\Permission;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\Menu;
use Validator;
use App\Http\Library\Serializer;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    private function getValidator($method, Request $request,$id=null)
    {
        if($method == 'menuStore'){
            return \Illuminate\Support\Facades\Validator::make($request->all(),[
                'menu_json' => 'required',
            ]);
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::all();
        $menu = [];
        foreach($menus as $menu_data){
            if($menu_data->parent_id != 0){
                $data_child = [
                    'name' => $menu_data->name,
                    'display_name' => $menu_data->display_name,
                    'icon' => $menu_data->icon,
                    'url' => $menu_data->url,
                    'permission_id' => $menu_data->permission_id
                ];
                $menu[$menu_data->parent_id]['children'][] = $data_child;
            }else{
                $menu[$menu_data->id] = [
                    'name' => $menu_data->name,
                    'display_name' => $menu_data->display_name,
                    'icon' => $menu_data->icon,
                    'url' => $menu_data->url,
                    'permission_id' => $menu_data->permission_id
                ];
            }
        }

        $menu = array_values($menu);
        $menu_json = json_encode($menu);
        $permission_show = Permission::where('name', 'like', '%show%')->get();
        return view('acl.menu.index', compact('menu', 'permission_show', 'menu_json'));
    }

    public function menuStore(Request $request)
    {
        $validator = $this->getValidator('menuStore',$request);

        if($validator->fails()){
            $messages = implode(',',array_column($validator->messages()->toArray(),0));

            $resource = Serializer::serializeItem(false,$messages);
            $statusCode = 422;
            return response()->json($resource,$statusCode);
        }else{
            $messages = "Data Menu tidak terdapat pada request";
            $menu_json = $request->menu_json;
            $menus = json_decode($menu_json);

            if(count($menus) > 0){
                $messages = "Success";
                $seq = 1;
                DB::table('menus')->truncate();

                foreach($menus as $menu){
                    DB::beginTransaction();
                    try {
                        $menu_new = new Menu();
                        $menu_new->name = $menu->name;
                        $menu_new->permission_id = $menu->permission_id;
                        $menu_new->parent_id = 0;
                        $menu_new->display_name = $menu->display_name;
                        $menu_new->icon = $menu->icon;
                        $menu_new->url = $menu->url;
                        $menu_new->seq = $seq;
                        $menu_new->save();

                        //save menu children
                        if (!empty($menu->children)) {
                            $seq_child = 1;
                            foreach ($menu->children as $child) {
                                $menu_child_new = new Menu();
                                $menu_child_new->parent_id = $menu_new->id;
                                $menu_child_new->seq = $seq_child;
                                $menu_child_new->permission_id = $child->permission_id;
                                $menu_child_new->name = $child->name;
                                $menu_child_new->display_name = $child->display_name;
                                $menu_child_new->icon = $child->icon;
                                $menu_child_new->url = $child->url;
                                $menu_child_new->save();
                                $seq_child++;
                            }
                        }

                        $seq++;
                        DB::commit();
                    } catch (\Exception $e) {
                        DB::rollback();
                        $messages = $e;
                        //throw $e;
                    } catch (\Throwable $e) {
                        DB::rollback();
                        $messages = $e;
                        //throw $e;
                    }
                }
            }

            $resource = Serializer::serializeItem(true,$messages);
            $statusCode = Response::HTTP_OK;
            return response()->json($resource,$statusCode);
        }

    }

}
