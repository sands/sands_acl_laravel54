<?php

namespace App\Http\Controllers\AdminAcl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Permission;
use App\models\Role;
use Validator;
use App\Http\Library\Serializer;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::all();

        $message = "success";
        $statusCode = 200;
        $resource = Serializer::serializeCollection(true,$message,$permissions);

        return response()->json($resource,$statusCode);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = Permission::find($id);
        $message = "success";
        $statusCode = 200;
        $resource = Serializer::serializeItem(true,$message,$permission);

        return response()->json($resource,$statusCode);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function attachPermission(Request $request)
    {
        $validator = $this->getValidator('attach',$request);

        if($validator->fails()){
            $messages = implode(',',array_column($validator->messages()->toArray(),0));

            $resource = Serializer::serializeItem(false,$messages);
            $statusCode = 422;
            return response()->json($resource,$statusCode);
        }else{
            $role = Role::find($request->role_id);
            $permission = Permission::find($request->permission_id);
            $role->attachPermission($permission);

            $message = "success";
            $statusCode = 200;
            $resource = Serializer::serializeItem(true,$message);
            return response()->json($resource,$statusCode);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detachPermission(Request $request)
    {
        $validator = $this->getValidator('detach',$request);

        if($validator->fails()){
            $messages = implode(',',array_column($validator->messages()->toArray(),0));

            $resource = Serializer::serializeItem(false,$messages);
            $statusCode = 422;
            return response()->json($resource,$statusCode);
        }else{
            $role = Role::find($request->role_id);
            $permission = Permission::find($request->permission_id);
            $role->detachPermission($permission);

            $message = "success";
            $statusCode = 200;
            $resource = Serializer::serializeItem(true,$message);
            return response()->json($resource,$statusCode);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function getValidator($method, Request $request,$id=null)
    {
        if($method == 'attach'){
            return Validator::make($request->all(),[
                // 'permission_id' => ['required',
                // Role::unique('permissions')->where(function($query){
                //     return $query->where('permission_id',$request->permission_id)
                //                 ->where('role_id',$role_id);
                // })],
                // 'role_id' => 'required|string|max:255',
            ]);
        }else if ($method =='detach'){
            return Validator::make($request->all(),[
                // 'permission_id' => ['required',
                // Role::unique('permissions')->where(function($query){
                //     return $query->where('permission_id',$request->permission_id)
                //                 ->where('role_id',$role_id);
                // })],
                // 'role_id' => 'required|string|max:255',
            ]);
        }

    }




}
