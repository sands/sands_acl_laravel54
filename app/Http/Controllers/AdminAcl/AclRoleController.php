<?php
/*
** Author : Sands, muhammad.arisandi@mncgroup.com
** Date   : November 2020
*/
namespace App\Http\Controllers\AdminAcl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Role;
use App\Models\User;
use Auth;
use App\Http\Library\Serializer;

class AclRoleController extends Controller
{
    public function rules($type)
    {
        switch($type) {
            case 'roleStore':
                return [
                    'name'=>'required',
                    'display_name'=>'required',
                    'description'=>'required',
                ];
                break;

            default:
                return [
                    'id'=>'required',
                ];
        }
    }

    public function role()
    {
        $roles = Role::all();
        return view('acl.role.index', compact('roles'));
    }

    public function roleShow(Role $role)
    {
        return view('acl.role._show', compact('role'));
    }

    public function roleStore(Request $request)
    {
        $validator = Validator::make($request->all(),  $this->rules('roleStore'));
        $status_auth = false;

        if ($validator->fails()) {
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));
            return redirect()->back()->with('msg', $messages);
        }else{
            DB::beginTransaction();
            try {
                $messages =  "Gagal Simpan Data";

                $role = new Role;
                $max_id = Role::max('id');
                if($max_id != null){
                    $role->id = $max_id+1;
                }else{
                    $role->id = 1;
                }
                $role->name = $request->name;
                $role->display_name = $request->display_name;
                $role->description = $request->description;

                if($role->save()){
                    $messages = "Berhasil Simpan Data";
                    DB::commit();
                }
            } catch (\Exception $e) {
                DB::rollback();
                $messages = $e;
                //throw $e;
            } catch (\Throwable $e) {
                DB::rollback();
                $messages = $e;
                //throw $e;
            }

            return redirect()->back()->with('msg', $messages);
        }

    }

    public function roleUpdate(Role $role, Request $request)
    {
        $validator = Validator::make($request->all(),  $this->rules('roleStore'));
        $messages = "";

        if($validator->fails()){
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));
        }else {
            DB::beginTransaction();
            try {
                $messages =  "Gagal Simpan Data";
                $role->name = $request->name;
                $role->display_name = $request->display_name;
                $role->description = $request->description;

                if($role->save()){
                    $messages = "Berhasil Simpan Data";
                    DB::commit();
                }
            } catch (\Exception $e) {
                DB::rollback();
                $messages = $e;
                //throw $e;
            } catch (\Throwable $e) {
                DB::rollback();
                $messages = $e;
                //throw $e;
            }
        }

        return redirect()->back()->with('msg', $messages);
    }

    public function roleDelete(Role $role)
    {
        $statusCode = Response::HTTP_BAD_REQUEST;
        $messages = '';
        $status = false;

        DB::beginTransaction();
        try {
            $messages = "Delete Data Tidak Berhasil, Data tidak ditemukan";
            if(!empty($role) && $role->delete()){
                DB::commit();
                $messages = "Delete Data Berhasil";
                $statusCode = Response::HTTP_OK;
                $status = true;
            }
        } catch (\Exception $e) {
            DB::rollback();
            $messages = $e;
            //throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            $messages = $e;
            //throw $e;
        }

        $resource = Serializer::serializeItem($status, $messages);
        return response()->json($resource,$statusCode);
    }

}