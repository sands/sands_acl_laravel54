<?php

namespace App\Http\Controllers\AdminAcl;

use App\Models\Permission;
use App\Models\PermissionRole;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Module;
use App\Models\Role;
use Validator;
use App\Http\Library\Serializer;
use Illuminate\Support\Facades\DB;

class ModuleController extends Controller
{
    private function getValidator($method, Request $request,$id=null)
    {
        if($method == 'moduleStore'){
            return \Illuminate\Support\Facades\Validator::make($request->all(),[
                'name' => 'required',
                'display_name' => 'required'
            ]);
        }elseif($method == 'moduleUpdate'){
            return \Illuminate\Support\Facades\Validator::make($request->all(),[
                'name' => 'required',
                'display_name' => 'required',
                'status' => 'required'
            ]);
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Module::with('permissions')->get();
        return view('acl.module.index', compact('modules'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Module $module)
    {
        if(!$module){
            $message = "Module Not Found";
            $statusCode = Response::HTTP_BAD_REQUEST;
            $resource = Serializer::serializeCollection(false,$message);
            return response()->json($resource,$statusCode);
        }

        return view('acl.module._show', compact('module'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getTableData(Request $request,$role_id)
    {
        //check
        if(Role::find($role_id) === null){
            $message = "Role with id: ".$role_id.", does not exist";
            $statusCode = 500;
            $resource = Serializer::serializeItem(false,$message);
            return response()->json($resource,$statusCode);
        }
        if($request->has('module_id')){
            $module_id = $request->module_id;
            if(Module::find($module_id) === null){
                $message = "Module with id: ".$module_id.", does not exist";
                $statusCode = 500;
                $resource = Serializer::serializeItem(false,$message);
                return response()->json($resource,$statusCode);
            }
        }else{
            $module_id = null;
        }


        $query = "SELECT z.id,z.name, IF(a.name IS NULL,0,1) as show_bool, a.id as \"show\", IF(b.name IS NULL,0,1) as create_bool ,
        b.id as \"create\",IF(c.name IS NULL,0,1) as update_bool, c.id as \"update\",IF(d.name IS NULL,0,1) delete_bool, d.id as \"delete\" 
        from modules z
        left join
        (select a.id,a.display_name, IF(b.permission_id IS NULL,null,a.name) as name,a.module_id from (select id, name, module_id, display_name from permissions where name like 'show%' ) a
        left join permission_role b on a.id = b.permission_id and role_id = ".$role_id.") a on z.id = a.module_id
        left join
        (select a.id,a.display_name, IF(b.permission_id IS NULL,null,a.name) as name,a.module_id from (select id, name, module_id, display_name from permissions where name like 'create%' ) a
        left join permission_role b on a.id = b.permission_id and role_id = ".$role_id.") b on z.id = b.module_id
        left join
        (select a.id,a.display_name, IF(b.permission_id IS NULL,null,a.name) as name,a.module_id from (select id, name, module_id, display_name from permissions where name like 'update%' ) a
        left join permission_role b on a.id = b.permission_id and role_id = ".$role_id.") c on z.id = c.module_id
        left join
        (select a.id,a.display_name, IF(b.permission_id IS NULL,null,a.name) as name,a.module_id from (select id, name, module_id, display_name from permissions where name like 'delete%' ) a
        left join permission_role b on a.id = b.permission_id and role_id = ".$role_id.") d on z.id = d.module_id";


        if($module_id != null) $query .= "where z.id = ".$module_id;

        $query .= " order by z.id asc";
        $data = DB::select(DB::raw($query));

        //Remap Data For Show In Datatable
        $data = collect($data)->map(function($v, $k){
            $v->no = ++$k;
            $checked_show = ($v->show_bool == 1) ? " CHECKED " : "";
            $checked_update = ($v->update_bool == 1) ? " CHECKED " : "";
            $checked_create = ($v->create_bool == 1) ? " CHECKED " : "";
            $checked_delete = ($v->delete_bool == 1) ? " CHECKED " : "";

            $disabled_show = empty($v->show) ? " DISABLED " : "";
            $disabled_update = empty($v->update) ? " DISABLED " : "";
            $disabled_create = empty($v->create) ? " DISABLED " : "";
            $disabled_delete = empty($v->delete) ? " DISABLED " : "";

            $disabled_show_class_label = empty($v->show) ? " checkbox-disabled " : "";
            $disabled_update_class_label = empty($v->update) ? " checkbox-disabled " : "";
            $disabled_create_class_label = empty($v->create) ? " checkbox-disabled " : "";
            $disabled_delete_class_label = empty($v->delete) ? " checkbox-disabled " : "";

            $el_checkbox_show = "<label class=\"checkbox checkbox-lg $disabled_show_class_label\">
                                <input type=\"checkbox\" ".$checked_show." ".$disabled_show." class=\"checkboxPerm\" value=\" ".$v->show." \">
                                <span></span>
                            </label>";
            $el_checkbox_update = "<label class=\"checkbox checkbox-lg $disabled_update_class_label\">
                                <input type=\"checkbox\" ".$checked_update." ".$disabled_update." class=\"checkboxPerm\" value=\" ".$v->update." \">
                                <span></span>
                            </label>";
            $el_checkbox_create = "<label class=\"checkbox checkbox-lg $disabled_create_class_label\">
                                <input type=\"checkbox\" ".$checked_create." ".$disabled_create." class=\"checkboxPerm\" value=\" ".$v->create." \">
                                <span></span>
                            </label>";
            $el_checkbox_delete = "<label class=\"checkbox checkbox-lg $disabled_delete_class_label\">
                                <input type=\"checkbox\" ".$checked_delete." ".$disabled_delete." class=\"checkboxPerm\" value=\" ".$v->delete." \">
                                <span></span>
                            </label>";

            $v->checked_show = $el_checkbox_show;
            $v->checked_update = $el_checkbox_update;
            $v->checked_create = $el_checkbox_create;
            $v->checked_delete = $el_checkbox_delete;

            return $v;
        });


        $records_total = count($data);
        $result['data'] = $data;
        $result['recordsTotal'] = $records_total;
        $result['recordsFiltered'] = $records_total;
        $statusCode = Response::HTTP_OK;

        return response()->json($result, $statusCode);

    }

    public function moduleStore(Request $request)
    {
        $validator = $this->getValidator('moduleStore',$request);

        if($validator->fails()){
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));
            return redirect()->back()->with('msg', $messages);
        }else{
            DB::beginTransaction();
            try {
                $permissions = $request->permissions;
                if(count($permissions) > 0){
                    $module = new Module();
                    $module->name = strtolower(str_replace(' ', '',  $request->name));
                    $module->display_name = $request->display_name;
                    $module->description = !empty($request->description) ?  $request->description : '';
                    $module->save();

                    foreach($permissions as $k => $permission){
                        $permission_name = $k.'-'.$module->name;
                        $display_name = ucwords($k.' '.$module->name);

                        $module_id = $module->id;
                        DB::table('permissions')->insert([
                            'name' => $permission_name,
                            'display_name' => $display_name,
                            'description' => $display_name,
                            'module_id' => $module_id,
                        ]);
                    }

                    $messages = "Data Berhasil Disimpan";
                }else{
                    $messages = "Data Gagal Disimpan, Permission Belum Dipilih";
                }

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $messages = $e;
                //throw $e;
            } catch (\Throwable $e) {
                DB::rollback();
                $messages = $e;
                //throw $e;
            }

            return redirect()->back()->with('msg', $messages);
        }
    }

    public function moduleUpdate(Module $module, Request $request)
    {
        $validator = $this->getValidator('moduleUpdate',$request);

        if($validator->fails()){
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));
            return redirect()->back()->with('msg', $messages);
        }else{
            DB::beginTransaction();
            try {
                $permissions = $request->permissions;
                if(count($permissions) > 0){
                    $module->name = strtolower(str_replace(' ', '',  $request->name));
                    $module->display_name = $request->display_name;
                    $module->status = $request->status;
                    $module->description = !empty($request->description) ?  $request->description : '';
                    $module->save();

                    //delete permission and permission role existing
                    $exist_permission = $module->permissions;
                    foreach($exist_permission as $perm){
                        $perm_exist_roles_delete = PermissionRole::where('permission_id', $perm->id)->delete();
                    }
                    $perm__exist_delete = Permission::where('module_id', $module->id)->delete();

                    foreach($permissions as $k => $permission){
                        $permission_name = $k.'-'.$module->name;
                        $display_name = ucwords($k.' '.$module->name);

                        $module_id = $module->id;
                        DB::table('permissions')->insert([
                            'name' => $permission_name,
                            'display_name' => $display_name,
                            'description' => $display_name,
                            'module_id' => $module_id,
                        ]);
                    }

                    $messages = "Data Berhasil Disimpan, Harap Memberikan Auth Ulang Pada Module yang baru diubah dengan menuju menu Manage Authorization";
                }else{
                    $messages = "Data Gagal Disimpan, Permission Belum Dipilih";
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $messages = $e;
                //throw $e;
            } catch (\Throwable $e) {
                DB::rollback();
                $messages = $e;
                //throw $e;
            }

            return redirect()->back()->with('msg', $messages);
        }
    }

}
