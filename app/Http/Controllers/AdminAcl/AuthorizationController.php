<?php

namespace App\Http\Controllers\AdminAcl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Permission;
use App\models\Role;
use Validator;
use App\Http\Library\Serializer;

class AuthorizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return view('acl.authorization.index', compact('roles'));
    }



}
