<?php
/*
** Author : Sands, muhammad.arisandi@mncgroup.com
** Date   : November 2020
*/
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Role;
use App\Models\User;
use Auth;
use App\Http\Library\Serializer;

class AuthenticateController extends Controller
{
    public function rules($type)
    {
        switch($type) {
            case 'loginProc':
                return [
                    'username'=>'required',
                    'password'=>'required',
                ];
                break;

            default:
                return [
                    'id'=>'required',
                ];
        }
    }

    public function login()
    {
        return view('auth.login');
    }

    public function loginProc(Request $request)
    {
        $validator = Validator::make($request->all(),  $this->rules('loginProc'));
        $status_auth = false;

        if ($validator->fails()) {
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));
            $resource = Serializer::serializeItem($status_auth, $messages);

            $statusCode = \Illuminate\Http\Response::HTTP_BAD_REQUEST;
        }else{
            $username = $request->input('username');
            $password = $request->input('password');
            $user = User::where([
                        ['username' , $username]
                    ])
                    ->first();
            $credentials = [
                'username' => $username,
                'password' => $password,
            ];

            if (!$user) {
                $messages = "Akun Anda Tidak Ditemukan";
                $data = null;
            }else{
                $data = $user;
                $is_superadmin = $user->hasRole('super-admin');
                if (!$is_superadmin) {
                    $conn_ldap = $this->check_ldap($username, $password) == true ? true : false;
                    $messages = $conn_ldap == false ? "Akun Tidak ditemukan di LDAP" : "Login Berhasil";
                    $status_auth = $conn_ldap;
                    if($conn_ldap) Auth::login($user);
                }else{
                    $login_admin = Auth::attempt($credentials);
                    $messages = $login_admin == false ? "Akun Tidak ditemukan di Database" : "Login Berhasil";
                    $status_auth = $login_admin;
                    if($login_admin) Auth::login($user);
                }
            }

            $statusCode = Response::HTTP_OK;
            $resource = Serializer::serializeItem($status_auth, $messages, $data);
        }

        return response()->json($resource,$statusCode);
    }

    private function check_ldap($username, $password)
    {
        // return true;
        $ldap_ip = '172.18.8.10';
        $ldap_port = 389;
        $ldap_domain = "@mncgroup.com";

        $ds = ldap_connect($ldap_ip, $ldap_port) or die("Could not connect");

        if ($ds)
        {
            try
            {
                $usercode = $username . $ldap_domain;
                $ldapbind = @ldap_bind($ds, $usercode, $password);
                // $ldap_dn = 'OU=MNCMedia,DC=mncgroup,DC=com';
                // $results = @ldap_search($ds, $ldap_dn, "(mail=$usercode)");
                // $entries = @ldap_get_entries($ds, $results);

                if (@$ldapbind)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception $e)
            {
                return false;
            }
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/login');
    }
}