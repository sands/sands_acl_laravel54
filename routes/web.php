<?php
/*
** Author : Sands, muhammad.arisandi@mncgroup.com
** Date   : November 2020
*/

Route::get('/sands/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

// Dashboard
Route::get('/', function(){
    return redirect('/home');
});

//Auth
Route::group(['as' => 'auth.'], function () {
    Route::get('login', 'AuthenticateController@login')->name('login');
    Route::post('login', 'AuthenticateController@loginProc')->name('login.proc');
});


Route::group(['middleware' => ['auth']], function() {
    Route::get('logout', 'AuthenticateController@logout')->name('logout');

    //Home
    Route::get('/home', 'HomeController@index')->name('home.index');

    //ACL
    Route::group(['prefix' => 'acl', 'as' => 'acl.'], function () {
        /***** ROLE ******/
        Route::middleware('permission:show-role')->get('role', 'AdminAcl\AclRoleController@role')->name('role');
        Route::middleware('permission:show-role')->get('role/{role}', 'AdminAcl\AclRoleController@roleShow')->name('role.show');
        Route::middleware('permission:create-role')->post('role', 'AdminAcl\AclRoleController@roleStore')->name('role.store');
        Route::middleware('permission:update-role')->post('role/{role}', 'AdminAcl\AclRoleController@roleUpdate')->name('role.update');
        Route::middleware('permission:delete-role')->delete('role/{role}', 'AdminAcl\AclRoleController@roleDelete')->name('role.delete');

        /***** USER ******/
        Route::middleware('permission:show-user')->get('user', 'AdminAcl\UserController@index')->name('user');
        Route::middleware('permission:show-user')->get('user/{user}', 'AdminAcl\UserController@userShow')->name('user.show');
        Route::middleware('permission:create-user')->post('user', 'AdminAcl\UserController@userStore')->name('user.store');
        Route::middleware('permission:update-user')->post('user/{user}', 'AdminAcl\UserController@userUpdate')->name('user.update');
        Route::middleware('permission:delete-user')->delete('user/{user}', 'AdminAcl\UserController@userDelete')->name('user.delete');

        /***** AUTHORIZATION ******/
        Route::get('/authorization','AdminAcl\AuthorizationController@index')->name('authorization');

        /***** MODULE ******/
        Route::get('/module','AdminAcl\ModuleController@index')->name('module');
        Route::get('/module/gettabledata/{role_id}','AdminAcl\ModuleController@getTableData')->name('module.gettabledata'); //show per role
        Route::get('/module/{module}','AdminAcl\ModuleController@show')->name('module.show');
        Route::post('/module','AdminAcl\ModuleController@moduleStore')->name('module.store');
        Route::post('/module/{module}','AdminAcl\ModuleController@moduleUpdate')->name('module.update');


        /***** MENU ******/
        Route::get('/menu','AdminAcl\MenuController@index')->name('menu');
        Route::post('/menu','AdminAcl\MenuController@menuStore')->name('menu.store');

        /***** PERMISSION ******/
        Route::get('/permission','AdminAcl\PermissionController@index')->name('permission');
        Route::get('/permission/{id}','AdminAcl\PermissionController@show')->name('permission.show');
        Route::post('/permission/attachpermission','AdminAcl\PermissionController@attachPermission')->name('permission.attachperm');
        Route::post('/permission/detachpermission','AdminAcl\PermissionController@detachPermission')->name('permission.detachperm');
    });

});


