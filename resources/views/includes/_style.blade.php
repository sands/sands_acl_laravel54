<!--begin::Fonts-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"/>        <!--end::Fonts-->

<!--begin::Page Vendors Styles(used by this page)-->
<link href="{{ URL::to('assets_template/plugins/custom/fullcalendar/fullcalendar.bundle.css?v=7.0.6') }}" rel="stylesheet" type="text/css"/>
<!--end::Page Vendors Styles-->


<!--begin::Global Theme Styles(used by all pages)-->
<link href="{{ URL::to('assets_template/plugins/global/plugins.bundle.css?v=7.0.6') }}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::to('assets_template/plugins/custom/prismjs/prismjs.bundle.css?v=7.0.6') }}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::to('assets_template/css/style.bundle.css?v=7.0.6') }}" rel="stylesheet" type="text/css"/>
<!--end::Global Theme Styles-->

<!--begin::Layout Themes(used by all pages)-->

<link href="{{ URL::to('assets_template/css/themes/layout/header/base/light.css?v=7.0.6') }}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::to('assets_template/css/themes/layout/header/menu/light.css?v=7.0.6') }}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::to('assets_template/css/themes/layout/brand/dark.css?v=7.0.6') }}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::to('assets_template/css/themes/layout/aside/dark.css?v=7.0.6') }}" rel="stylesheet" type="text/css"/>        <!--end::Layout Themes-->

<link rel="shortcut icon" href="{{ URL::to('assets_template/media/logos/favicon.ico') }}"/>

<link href="{{ URL::to('assets_template/plugins/custom/datatables/datatables.bundle.css?v=7.0.6') }}" rel="stylesheet" type="text/css"/>