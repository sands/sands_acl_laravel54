<div class="modal-header">
    <h5 class="modal-title">Update User</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <i aria-hidden="true" class="ki ki-close"></i>
    </button>
</div>
<div class="modal-body">
    <form action="{{route('acl.user.update', ['user' => $user->id])}}" method="POST" id="formUpdate">
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label>Fullname</label>
            <input type="text" class="form-control" name="fullname" required placeholder="Enter Value" value="{{$user->fullname}}">
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" name="email" required placeholder="Enter Value" value="{{$user->email}}">
        </div>
        <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control" name="username" required placeholder="Enter Value" value="{{$user->username}}">
        </div>
        <div class="form-group">
            <label>Role</label>
            <select name="role_id" class="form-control">
                <option value="" selected disabled>== Pilih ==</option>
                @foreach($roles as $key => $role)
                    @php
                        $selected = ($role->id == $user->roles[0]->id) ? " SELECTED " : "";
                    @endphp
                    <option {{$selected}} value="{{$role->id}}"> {{$role->name}} </option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Status</label>
            <select name="status" class="form-control">
                @foreach($statusses as $key => $status)
                    @php
                    $selected = ($key == $user->status) ? " SELECTED " : "";
                    @endphp
                    <option {{$selected}} value="{{$key}}"> {{$status}} </option>
                @endforeach
            </select>
        </div>

</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary updateBtn">Update</button>
    </form>
</div>