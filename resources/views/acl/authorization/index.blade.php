@extends('layouts.master')

@section('title') Authorization @endsection

@section('content')
    <!--begin::Content-->
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
            <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">
                            Authorization
                        </h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="" class="text-muted">
                                    ACL
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="" class="text-muted">
                                    Authorization
                                </a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Actions-->
                    <a href=" {{ url()->previous() }} " class="btn btn-light-primary font-weight-bolder btn-sm">
                        Back
                    </a>
                    <!--end::Actions-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->

        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class=" container ">
                @if (\Session::has('msg'))
                    <div class="alert alert-custom alert-notice alert-light-primary fade show" role="alert">
                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                        <div class="alert-text">{!! \Session::get('msg') !!}</div>
                        <div class="alert-close">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="ki ki-close"></i></span>
                            </button>
                        </div>
                    </div>
                @endif

                <!--begin::Card-->
                <div class="row">
                    <div class="card card-custom bg-primary col-md-6">
                        <div class="card-header border-0">
                            <div class="card-title">
                            <span class="card-icon">
                                <i class="fa fa-search text-white"></i>
                            </span>
                                <h3 class="card-label text-white">
                                    Filter
                                </h3>
                            </div>
                            <div class="card-toolbar">
                            </div>
                        </div>
                        <div class="separator separator-solid separator-white opacity-20"></div>
                        <div class="card-body text-white">
                            <div class="form-group">
                                <label class="text-white">Role</label>
                                <select class="form-control" id="roleId">
                                    <option value="" selected disabled>== PILIH ==</option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}"> {{$role->name}} </option>
                                    @endforeach
                                </select>
                            </div>

                            <button type="button" class="btn btn-light btn-block" id="filterBtn">Filter</button>
                        </div>
                    </div>
                </div>


                <div class="card card-custom gutter-b mt-5">
                    <div class="card-header flex-wrap py-3">
                        <div class="card-title">
                            <h3 class="card-label">
                                List Module Per Role
                                <span class="d-block text-muted pt-2 font-size-sm">ACL</span>
                            </h3>
                        </div>
                        <div class="card-toolbar">


                        </div>
                    </div>
                    <div class="card-body">
                        <!--begin: Datatable-->
                        <table class="table table-bordered table-checkable" id="tableModule">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Module</th>
                                <th>Create</th>
                                <th>Read</th>
                                <th>Update</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                        <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>

    <!--end::Content-->
@endsection

@section('script')
    <script src="/js/acl/authorization/index.js?v=sands{{time()}}"></script>
@endsection