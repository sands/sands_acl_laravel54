<div class="modal-header">
    <h5 class="modal-title">Update Role</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <i aria-hidden="true" class="ki ki-close"></i>
    </button>
</div>
<div class="modal-body">
    <form action="{{route('acl.role.update', ['role' => $role->id])}}" method="POST" id="formUpdate">
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" name="name" required placeholder="Enter Value" value="{{$role->name}}">
        </div>
        <div class="form-group">
            <label>Display Name</label>
            <input type="text" class="form-control" name="display_name" required placeholder="Enter Value" value="{{$role->display_name}}">
        </div>
        <div class="form-group">
            <label>Description</label>
            <input type="text" class="form-control" name="description" required placeholder="Enter Value" value="{{$role->description}}">
        </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary updateBtn">Update</button>
    </form>
</div>