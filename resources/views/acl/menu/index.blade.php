@extends('layouts.master')

@section('title') Menus @endsection

@section('style')
    <link href="{{ URL::to('assets_template/bootstrap-iconpicker/css/bootstrap-iconpicker.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <!--begin::Content-->
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
            <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">
                            Menus
                        </h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="" class="text-muted">
                                    ACL
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="" class="text-muted">
                                    Menus
                                </a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Actions-->
                    <a href=" {{ url()->previous() }} " class="btn btn-light-primary font-weight-bolder btn-sm">
                        Back
                    </a>
                    <!--end::Actions-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->

        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class=" container ">
                @if (\Session::has('msg'))
                    <div class="alert alert-custom alert-notice alert-light-primary fade show" role="alert">
                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                        <div class="alert-text">{!! \Session::get('msg') !!}</div>
                        <div class="alert-close">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="ki ki-close"></i></span>
                            </button>
                        </div>
                    </div>
                @endif

                <!--begin::Card-->
                <div class="card card-custom gutter-b">
                    <div class="card-header flex-wrap py-3">
                        <div class="card-title">
                            <h3 class="card-label">
                                List Menus
                                <span class="d-block text-muted pt-2 font-size-sm">ACL</span>
                            </h3>
                        </div>
                        <div class="card-toolbar">
                            <!--begin::Button-->
                            <a href="#" id="saveMenusAllBtn" class="btn btn-primary font-weight-bolder">
                                    <span class="svg-icon svg-icon-md">
                                       <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                                       <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                          <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                             <rect x="0" y="0" width="24" height="24"/>
                                             <circle fill="#000000" cx="9" cy="15" r="6"/>
                                             <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"/>
                                          </g>
                                       </svg>
                                        <!--end::Svg Icon-->
                                    </span>
                                Save Menu
                            </a>
                            <!--end::Button-->
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <ul id="myEditor" class="sortableLists list-group">
                                </ul>
                            </div>

                            <div class="col-md-6">
                                <div class="card border-primary mb-3">
                                    <div class="card-header bg-primary text-white">Edit item</div>
                                    <div class="card-body">
                                        <form id="frmEdit" class="form-horizontal">
                                            <div class="form-group">
                                                <label for="text">Name</label>
                                                <input type="text" class="form-control item-menu" name="name" id="name" placeholder="Name">
                                            </div>

                                            <div class="form-group">
                                                <label for="title">Display Name</label>
                                                <input type="text" name="display_name" class="form-control item-menu" id="display_name" placeholder="Display Name">
                                            </div>

                                            <div class="form-group">
                                                <label for="title">Icon (class)</label>
                                                <input type="text" name="icon" class="form-control item-menu" id="icon" placeholder="Class Icon">
                                            </div>

                                            <div class="form-group">
                                                <label for="url">URL {{\URL::to('/')}}/... </label>
                                                <input type="text" name="url" class="form-control item-menu" id="url" placeholder="URL">
                                            </div>

                                            <div class="form-group">
                                                <label for="permission">Permission</label>
                                                <select name="permission_id" id="permission" class="item-menu form-control">
                                                    <option value="" selected disabled>== Pilih ==</option>
                                                    @foreach($permission_show as $perm)
                                                        <option value="{{$perm->id}}"> {{$perm->name}} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer">
                                        <button type="button" id="btnUpdate" class="btn btn-primary" disabled><i class="fas fa-sync-alt"></i> Update</button>
                                        <button type="button" id="btnAdd" class="btn btn-success"><i class="fas fa-plus"></i> Add</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>

    <!--end::Content-->
@endsection

@section('script')
    <script>
        jQuery(document).ready(function() {
            window.MENU_ARR =  JSON.parse('<?php echo $menu_json;?>');
        })
    </script>
    <script src="/js/acl/menu/index.js?v=sands{{time()}}"></script>

    <script src="{{ URL::to('assets_template/bootstrap-iconpicker/js/bootstrap-iconpicker.min.js') }}"></script>
    <script src="{{ URL::to('assets_template/js/jquery-menu-editor.js') }}"></script>
@endsection