
    <div class="modal-body">
        <form action="{{route('acl.module.update', ['module' => $module->id])}}" method="POST" id="formUpdate">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control moduleNameInput" name="name" value="{{$module->name}}" required placeholder="ex : data-master">
            </div>
            <div class="form-group">
                <label>Display Name</label>
                <input type="text" class="form-control" name="display_name" value="{{$module->display_name}}" required placeholder="ex: Data Master">
            </div>
            <div class="form-group">
                <label>Description</label>
                <input type="text" class="form-control" name="description"  value="{{$module->description}}" placeholder="Enter Description">
            </div>

            <div class="form-group">
                <label>Status</label>
                <select name="status" id="" class="form-control">
                    @php
                            $status_active_selected = ($module->status == 1) ? " SELECTED " : "";
                            $status_nonactive_selected = ($module->status == 0) ? " SELECTED " : "";
                    @endphp
                    <option {{$status_active_selected}} value="1">Active</option>
                    <option {{$status_nonactive_selected}} value="0">Non Active</option>
                </select>
            </div>

            <div class="form-group row">
                <label class="col-3 col-form-label">Permissions</label>
                <div class="col-9 col-form-label">
                    <div class="checkbox-list">
                        @php
                        $components_perm = ['show', 'update', 'delete', 'create'];
                        @endphp
                        @foreach($components_perm as $component_perm)
                            @php
                                $permissions = $module->permissions->pluck('name');

                                $new_permissions = [];
                                foreach($permissions as $k => $permission){
                                    $implode = explode('-', $permission);
                                    $arr_key_implode = array_keys($implode);
                                    $end_key = end($arr_key_implode);

                                    $this_perm = $implode[0];
                                    $this_perm_name = "";
                                    foreach($implode as $key => $perm_name){
                                        if($key != 0){
                                            $prefix_strip = ($key == $end_key) ? "" : "-";
                                            $this_perm_name .= $implode[$key].$prefix_strip;
                                        }
                                    }

                                    $new_permissions[$k]['type'] = $this_perm;
                                    $new_permissions[$k]['permission'] = $permission;
                                    $new_permissions[$k]['permission_postfix'] = $this_perm_name;
                                }

                                $new_permissions = array_values($new_permissions);

                                $have_perm = array_search($component_perm, array_column($new_permissions, 'type'));

                               if($have_perm !== false){
                                   $these_perm = $new_permissions[$have_perm];
                                   $checked = "  CHECKED ";
                                   $label_check = $these_perm['type'].'-<div class="moduleName">'.$these_perm['permission_postfix'].'</div>';
                               }else{
                                   $get_postfix_from_firstdata = $new_permissions[0]['permission_postfix'];
                                   $checked = "";
                                   $label_check = $component_perm.'-<div class="moduleName">'.$get_postfix_from_firstdata.'</div>';
                               }

                               $html_check = '<label class="checkbox">
                                                <input type="checkbox" '.$checked.' name="permissions['.$component_perm.']">
                                                <span></span>
                                                '.$label_check.' </label>';
                            @endphp
                            {!! $html_check !!}
                        @endforeach

                    </div>
                </div>
            </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btnUpdateSubmit">Update</button>
        </form>
    </div>
