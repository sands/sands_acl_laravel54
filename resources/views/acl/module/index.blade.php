@extends('layouts.master')

@section('title') Modules @endsection

@section('content')
    <!--begin::Content-->
    <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
            <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">
                            Modules
                        </h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="" class="text-muted">
                                    ACL
                                </a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="" class="text-muted">
                                    Modules
                                </a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Actions-->
                    <a href=" {{ url()->previous() }} " class="btn btn-light-primary font-weight-bolder btn-sm">
                        Back
                    </a>
                    <!--end::Actions-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->

        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class=" container ">
                @if (\Session::has('msg'))
                    <div class="alert alert-custom alert-notice alert-light-primary fade show" role="alert">
                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                        <div class="alert-text">{!! \Session::get('msg') !!}</div>
                        <div class="alert-close">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="ki ki-close"></i></span>
                            </button>
                        </div>
                    </div>
                @endif

                <!--begin::Card-->
                <div class="row">
                    <div class="card card-custom wave wave-animate-slow col-md-4 ml-3">
                        <div class="card-body">
                            <div class="d-flex align-items-center p-5">
                                <div class="mr-6">
                                    <span class="svg-icon svg-icon-warning svg-icon-4x">
                                        <svg>
                                            ...
                                        </svg>
                                    </span>
                                </div>
                                <div class="d-flex flex-column">
                                    <a href="#" class="text-dark text-hover-primary font-weight-bold font-size-h4 mb-3">
                                        Manage Module
                                    </a>
                                    <div class="text-dark-75">
                                        <a href="#" id="newModuleBtn" class="btn btn-lg btn-primary font-weight-bolder">
                                            <span class="svg-icon svg-icon-md">
                                               <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                                               <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                  <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                     <rect x="0" y="0" width="24" height="24"/>
                                                     <circle fill="#000000" cx="9" cy="15" r="6"/>
                                                     <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"/>
                                                  </g>
                                               </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                            New Module
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="card card-custom gutter-b mt-5">
                    <div class="card-header flex-wrap py-3">
                        <div class="card-title">
                            <h3 class="card-label">
                                List Module
                                <span class="d-block text-muted pt-2 font-size-sm">ACL</span>
                            </h3>
                        </div>
                        <div class="card-toolbar">


                        </div>
                    </div>
                    <div class="card-body">
                        <!--begin: Datatable-->
                        <table class="table table-bordered table-checkable" id="tableModule">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Module</th>
                                <th>Permissions</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $no = 1;
                            @endphp
                            @foreach($modules as $module)
                                @php
                                $permissions = $module->permissions->pluck('name');
                                $permissions_str = "";
                                foreach($permissions as $permission){
                                    $permissions_str .= $permission.", ";
                                }
                                @endphp
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{$module->name}}</td>
                                    <td>{{$permissions_str}}</td>
                                    <td>
                                        <button class="btn btn-success btn-sm btnUpdate" data-id="{{$module->id}}">Update</button>
                                    </td>
                                </tr>
                                @php
                                    $no++;
                                @endphp
                            @endforeach
                            </tbody>
                        </table>
                        <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>

    {{--    add modal--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="modalModule">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Module</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('acl.module.store')}}" method="POST" id="formCreate">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="moduleNameInput form-control" name="name" required placeholder="ex : data-master">
                        </div>
                        <div class="form-group">
                            <label>Display Name</label>
                            <input type="text" class="form-control" name="display_name" required placeholder="ex: Data Master">
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <input type="text" class="form-control" name="description"  placeholder="Enter Description">
                        </div>
                        <div class="form-group row">
                                <label class="col-3 col-form-label">Permissions</label>
                                <div class="col-9 col-form-label">
                                    <div class="checkbox-list">
                                        <label class="checkbox">
                                            <input type="checkbox" name="permissions[show]">
                                            <span></span>
                                            show-<div class="moduleName"></div>
                                        </label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="permissions[create]">
                                            <span></span>
                                            create-<div class="moduleName"></div>
                                        </label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="permissions[update]">
                                            <span></span>
                                            update-<div class="moduleName"></div>
                                        </label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="permissions[delete]">
                                            <span></span>
                                            delete-<div class="moduleName"></div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{--    edit modal--}}
    <div class="modal fade" tabindex="-1" role="dialog" id="modalUpdateModule">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Module</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modalModuleEditContent"></div>
            </div>
        </div>
    </div>
    <!--end::Content-->
@endsection

@section('script')
    <script src="/js/acl/module/index.js?v=sands{{time()}}"></script>
@endsection