<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //CREATE USERS
        DB::table('users')->insert([
            'id'=>1,
            'fullname' => 'Administrator',
            'username' => 'admin',
            'email' => 'admin@covid.mnc',
            'password' => bcrypt('admin'),
        ]);

        //ATTACH USER_ROLE
        $user = User::where('email','admin@covid.mnc')->first();
        $superAdmin = Role::where('name','super-admin')->first();
        $user->attachRole($superAdmin);
    }
}