<?php

use Illuminate\Database\Seeder;
use App\Models\Module;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /***********SETTING MODULE*********/
        DB::table('modules')->insert([
            'id' => 3,
            'name' => 'dashboard',
            'display_name' => 'Dashboard',
            'description' => 'Dashboard',
        ]);
        DB::table('modules')->insert([
            'id' => 4,
            'name' => 'user',
            'display_name' => 'User Management',
            'description' => 'Manage User',
        ]);
        DB::table('modules')->insert([
            'id' => 5,
            'name' => 'role',
            'display_name' => 'Role Management',
            'description' => 'Manage Role',
        ]);
        DB::table('modules')->insert([
            'id' => 6,
            'name' => 'permission',
            'display_name' => 'Manage Permission',
            'description' => 'Manage Permission',
        ]);
        DB::table('modules')->insert([
            'id' => 7,
            'name' => 'authorization',
            'display_name' => 'Manage Authorization',
            'description' => 'Manage Authorization, Menu, Module',
        ]);
        DB::table('modules')->insert([
            'id' => 8,
            'name' => 'data-master',
            'display_name' => 'Data Master',
            'description' => 'Data Master',
        ]);

        /***********SETTING PERMISSION*****/
        $modules = Module::all();

        //DASHBOARD
        $module_id = 3;
        DB::table('permissions')->insert([
            'name' => 'show-dashboard',
            'display_name' => 'Show Dashboard',
            'description' => 'Show Dashboard',
            'module_id' => $module_id,
        ]);

        //USER
        $module_id = 4;
        DB::table('permissions')->insert([
            'name' => 'show-user',
            'display_name' => 'Show User',
            'description' => 'Show User',
            'module_id' => $module_id,
        ]);
        DB::table('permissions')->insert([
            'name' => 'create-user',
            'display_name' => 'Create User',
            'description' => 'Create new User',
            'module_id' => $module_id,
        ]);
        DB::table('permissions')->insert([
            'name' => 'update-user',
            'display_name' => 'Update User',
            'description' => 'Update User',
            'module_id' => $module_id,
        ]);
        DB::table('permissions')->insert([
            'name' => 'delete-user',
            'display_name' => 'Delete User',
            'description' => 'Delete User',
            'module_id' => $module_id,
        ]);

        //ROLE
        $module_id = 5;
        DB::table('permissions')->insert([
            'name' => 'show-role',
            'display_name' => 'Show Role',
            'description' => 'Show Role',
            'module_id' => $module_id,
        ]);
        DB::table('permissions')->insert([
            'name' => 'create-role',
            'display_name' => 'Create Role',
            'description' => 'Create new Role',
            'module_id' => $module_id,
        ]);
        DB::table('permissions')->insert([
            'name' => 'update-role',
            'display_name' => 'Update Role',
            'description' => 'Update Role',
            'module_id' => $module_id,
        ]);
        DB::table('permissions')->insert([
            'name' => 'delete-role',
            'display_name' => 'Delete Role',
            'description' => 'Delete Role',
            'module_id' => $module_id,
        ]);

        //PERMISSION
        $module_id = 6;
        DB::table('permissions')->insert([
            'name' => 'show-permission',
            'display_name' => 'Show ',
            'description' => 'Show Permission',
            'module_id' => $module_id,
        ]);
        DB::table('permissions')->insert([
            'name' => 'create-permission',
            'display_name' => 'Attach Permission',
            'description' => 'Attach Permission to Role',
            'module_id' => $module_id,
        ]);
        DB::table('permissions')->insert([
            'name' => 'delete-permission',
            'display_name' => 'Detach Permission',
            'description' => 'Detach Permission from Role',
            'module_id' => $module_id,
        ]);

        //AUTHORIZATION
        $module_id = 7;
        DB::table('permissions')->insert([
            'name' => 'show-authorization',
            'display_name' => 'Show ',
            'description' => 'Show Authorization',
            'module_id' => $module_id,
        ]);
        DB::table('permissions')->insert([
            'name' => 'create-authorization',
            'display_name' => 'Create Authorization',
            'description' => 'Create Authorization',
            'module_id' => $module_id,
        ]);
        DB::table('permissions')->insert([
            'name' => 'update-authorization',
            'display_name' => 'Update Authorization',
            'description' => 'Update Authorization',
            'module_id' => $module_id,
        ]);
        DB::table('permissions')->insert([
            'name' => 'delete-authorization',
            'display_name' => 'Delete Authorization',
            'description' => 'Delete Authorization',
            'module_id' => $module_id,
        ]);


        //DATA MASTER
        $module_id = 8;
        DB::table('permissions')->insert([
            'name' => 'show-data-master',
            'display_name' => 'Show Data Master',
            'description' => 'Show Data Master',
            'module_id' => $module_id,
        ]);
        DB::table('permissions')->insert([
            'name' => 'create-data-master',
            'display_name' => 'Show Data Master',
            'description' => 'Show Data Master',
            'module_id' => $module_id,
        ]);
        DB::table('permissions')->insert([
            'name' => 'update-data-master',
            'display_name' => 'Show Data Master',
            'description' => 'Show Data Master',
            'module_id' => $module_id,
        ]);
        DB::table('permissions')->insert([
            'name' => 'delete-data-master',
            'display_name' => 'Show Data Master',
            'description' => 'Show Data Master',
            'module_id' => $module_id,
        ]);

    }
}
