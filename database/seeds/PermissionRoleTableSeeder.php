<?php

use Illuminate\Database\Seeder;
use App\Models\Module;
use App\Models\Role;
use App\Models\Permission;
use App\Menu;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /***********SETTING ROLE*********/
        DB::table('roles')->insert([
            'id' => 1,
            'name' => 'super-admin',
            'display_name' => 'Super Admin',
            'description' => 'All Access',
        ]);
        DB::table('roles')->insert([
            'id' => 2,
            'name' => 'default',
            'display_name' => 'Default',
            'description' => 'User with read access',
        ]);


        /***********ATTACH ROLE PERMISSION*********/
        $permissions = Permission::all();
        $superAdmin = Role::where('name','super-admin')->first();
        $default = Role::where('name','default')->first();

        foreach($permissions as $permission){
            //SUPER ADMIN
            $superAdmin->attachPermission($permission);

            switch($permission->name){
                case 'show-dashboard':
                    $default->attachPermission($permission);
                    break;
            }
        }

    }
}
