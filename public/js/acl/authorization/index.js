"use strict";
var DatatableAuth = function() {
    var ajaxCsrfTokenInit = function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }



    var tableAuthInit = function() {
        var table = $('#tableModule');

        // begin first table
        table.DataTable({
            responsive: true,

            // DOM Layout settings
            dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

            lengthMenu: [5, 10, 25, 50],

            pageLength: 10,

            language: {
                'lengthMenu': 'Display _MENU_',
            },

            // Order settings
            order: [[0, 'asc']],
        });

    }



    var functionInit = function() {

    }



    var eventsInit = function() {
        $('#filterBtn').click(function() {
            let roleId = $('#roleId').val()

            if(roleId != null){
                KTApp.blockPage({
                    overlayColor: '#000000',
                    state: 'danger',
                    message: 'Please wait...'
                })

                $('#tableModule').DataTable().destroy();
                $('#tableModule').DataTable({
                    "bSort": false,
                    "pageLength": 25,
                    "fnInitComplete": function (oSettings, json) {
                        KTApp.unblockPage()
                        togglePermission()
                    },
                    "ajax": {
                        "url": '/acl/module/gettabledata/'+roleId,
                        "type": 'GET',
                        error: function (xhr, error, code) {
                            let errorMsg = xhr.responseJSON
                            if (xhr.status != 200) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Info',
                                    text: errorMsg,
                                })
                            }
                        },
                    },
                    "columns": [
                        { "data": "no" },
                        { "data": "name" },
                        { "data": "checked_create" },
                        { "data": "checked_show" },
                        { "data": "checked_update" },
                        { "data": "checked_delete" }
                    ]
                });
            }

        })

        function togglePermission(){
            $('.checkboxPerm').change(function(){
                KTApp.blockPage({
                    overlayColor: '#000000',
                    state: 'danger',
                    message: 'Please wait...'
                })

                let permId = $(this).val()
                let roleId = $('#roleId').val()
                let isChecked = $(this).is(':checked')
                let url = (isChecked) ? '/acl/permission/attachpermission' : '/acl/permission/detachpermission'

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        role_id : roleId ,
                        permission_id : permId ,
                    },
                    cache: false,
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        let errors = XMLHttpRequest.responseText
                        Swal.fire({
                            icon: 'error',
                            title: 'Info',
                            text: errors
                        })
                        KTApp.unblockPage()
                    }
                }).done(function (res) {
                    if(res.status) KTApp.unblockPage()
                })
            })
        }

    }



    return {
        //main function to initiate the module
        init: function() {
            ajaxCsrfTokenInit()
            tableAuthInit()
            eventsInit()
            functionInit()
        }
    };
}();

jQuery(document).ready(function() {
    DatatableAuth.init();
});
