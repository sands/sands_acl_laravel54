"use strict";
var DatatableRole = function() {
    var ajaxCsrfTokenInit = function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }

    var tableRoleInit = function() {
        var table = $('#tableRole');

        // begin first table
        table.DataTable({
            responsive: true,

            // DOM Layout settings
            dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

            lengthMenu: [5, 10, 25, 50],

            pageLength: 10,

            language: {
                'lengthMenu': 'Display _MENU_',
            },

            // Order settings
            order: [[0, 'asc']],
        });

    };

    var eventsInit = function() {
        $('#newRoleBtn').click(function () {
            $('#modalRole').modal('show')
        })

        $('#tableRole tbody').on('click', '.editBtn', function () {
            let id = $(this).data('id')
            $.ajax({
                url: '/acl/role/'+id,
                type: 'GET',
                dataType:'html',
                cache: false,
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    let errors = XMLHttpRequest.responseText
                    Swal.fire({
                        icon: 'error',
                        title: 'Info',
                        text: errors,
                    })
                }
            }).done(function (res) {
                $('.modalRoleEditContent').html(res)
                $('#modalRoleEdit').modal('show')
                submitUpdateBtn()
            })
        })

        $('#tableRole tbody').on('click', '.deleteBtn', function () {
            let id = $(this).data('id')

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    deleteData(id)
                }
            })
        })

        function submitUpdateBtn(){
            $('.updateBtn').click(function(){
                let form = $('#formUpdate')
                form.submit()
            })
        }

        function deleteData(id){
            $.ajax({
                url: '/acl/role/'+id,
                type: 'DELETE',
                cache: false,
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    let errors = XMLHttpRequest.responseText
                    Swal.fire({
                        icon: 'error',
                        title: 'Info',
                        text: errors
                    })
                }
            }).done(function (res) {
                let status = res.status,
                    message = res.message

                Swal.fire({
                    icon: 'info',
                    title: 'Info',
                    text: message
                })

                if(status){
                    location.reload()
                }
            })
        }
    }

    return {
        //main function to initiate the module
        init: function() {
            ajaxCsrfTokenInit()
            tableRoleInit()
            eventsInit()
        }
    };
}();

jQuery(document).ready(function() {
    DatatableRole.init();
});
