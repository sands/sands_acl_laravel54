"use strict";
var DatatableAuth = function() {
    var ajaxCsrfTokenInit = function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }



    var tableAuthInit = function() {
        var table = $('#tableModule');

        // begin first table
        table.DataTable({
            responsive: true,

            lengthMenu: [5, 10, 25, 50],

            pageLength: 10,

            language: {
                'lengthMenu': 'Display _MENU_',
            },

            // Order settings
            order: [[0, 'asc']],
        });

    }



    var functionInit = function() {

    }



    var eventsInit = function() {
        $('#newModuleBtn').click(function () {
            $('#modalModule').modal('show')
        })

        $('#tableModule tbody').on('click', '.btnUpdate', function () {
            let id = $(this).data('id')
            $.ajax({
                url: '/acl/module/'+id,
                type: 'GET',
                dataType:'html',
                cache: false,
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    let errors = XMLHttpRequest.responseText
                    Swal.fire({
                        icon: 'error',
                        title: 'Info',
                        text: errors,
                    })
                }
            }).done(function (res) {
                $('.modalModuleEditContent').html(res)
                $('#modalUpdateModule').modal('show')
                keyUpModuleName()
                updateSubmitInit()
            })
        })

        function keyUpModuleName(){
            $('.moduleNameInput').keyup(function(){
                let v = $(this).val()
                $('.moduleName').text(v)
            })
        }

        function updateSubmitInit(){
            $('.btnUpdateSubmit').click(function(){
                $('#formUpdate').submit()
            })
        }

        function updateDataInit(){
            $('.btnUpdateModuleShow').click(function(){
                KTApp.blockPage({
                    overlayColor: '#000000',
                    state: 'danger',
                    message: 'Please wait...'
                })


                let id = $(this).data('moduleid')
                let url = '/acl/module/'+id

                $.ajax({
                    url: url,
                    type: 'GET',
                    cache: false,
                    typeData: 'html',
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        let errors = XMLHttpRequest.responseText
                        Swal.fire({
                            icon: 'error',
                            title: 'Info',
                            text: errors
                        })
                        KTApp.unblockPage()
                    }
                }).done(function (res) {
                    KTApp.unblockPage()
                    $('.modalModuleEditContent').html(res)
                    $('#modalUpdateModule').modal('show')
                    keyUpModuleName()
                })
            })
        }

        keyUpModuleName()
        updateDataInit()
    }



    return {
        //main function to initiate the module
        init: function() {
            ajaxCsrfTokenInit()
            tableAuthInit()
            eventsInit()
            functionInit()
        }
    };
}();

jQuery(document).ready(function() {
    DatatableAuth.init();
});
