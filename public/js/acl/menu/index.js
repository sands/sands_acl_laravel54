"use strict";
var MenuScript = function() {
    var ajaxCsrfTokenInit = function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }



    var eventsInit = function() {


    }

    var dragdropMenuInit = function() {
        // sortable list options
        var sortableListOptions = {
            placeholderCss: {'background-color': "#cccccc"}
        };
        var editor = new MenuEditor('myEditor',
            {
                listOptions: sortableListOptions,
                maxLevel: 2 // (Optional) Default is -1 (no level limit)
                // Valid levels are from [0, 1, 2, 3,...N]
            });
        editor.setForm($('#frmEdit'));
        editor.setUpdateButton($('#btnUpdate'));
        //Calling the update method
        $("#btnUpdate").click(function(){
            editor.update();
        });
        // Calling the add method
        $('#btnAdd').click(function(){
            editor.add();
        });

        $('#saveMenusAllBtn').click(function(){
            var str = editor.getString()

            Swal.fire({
                title: 'Save Menu?',
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Submit'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '/acl/menu',
                        type: 'POST',
                        data: {
                            menu_json : str
                        },
                        cache: false,
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            let errors = XMLHttpRequest.responseText
                            Swal.fire({
                                icon: 'error',
                                title: 'Info',
                                text: errors
                            })
                        }
                    }).done(function (res) {
                        let status = res.status,
                            message = res.message

                        Swal.fire({
                            icon: 'info',
                            title: 'Info',
                            text: message
                        })

                        if(status){
                            location.reload()
                        }
                    })
                }
            })


        })

        //SAMPLE MENU
        var arrayJson = [{"name":"dashboard","display_name":"Dashboard","icon":"fas fa-align-justify","permission_id":"1", "url" : "dashboard"},
            {"name":"adminstrator","display_name":"Adminstrator","icon":"fas fa-align-justify","permission_id":"2", "url" : "",
                "children":[{"name":"manage-user","display_name":"Manage User","icon":"","permission_id":"2", "url" : "acl/user"}]}
                ]

        //SET MENU
        editor.setData(MENU_ARR)
    }

    return {
        //main function to initiate the module
        init: function() {
            ajaxCsrfTokenInit()
            eventsInit()
            dragdropMenuInit()
        }
    };
}();

jQuery(document).ready(function() {
    MenuScript.init();
});
